package main

import (
	"context"
	"net/http"
	"log"
	"github.com/gorilla/mux"
	"github.com/Diggernaut/mxj"
	"os"
	"os/signal"
	"time"
	"fmt"
	"io/ioutil"
	"strings"
	"mime"
	"encoding/json"
	"github.com/pkg/errors"
)

const ApiUrl = "http://jsonplaceholder.typicode.com"

func getHttpClient() *http.Client {
	tr := &http.Transport{
		MaxIdleConnsPerHost: 1024,
		Proxy:               http.ProxyFromEnvironment,
	}

	// init client
	c := &http.Client{
		Transport: tr,
		Timeout:   time.Duration(100) * time.Second,
	}

	return c
}

func hasContentType(r *http.Request, mimetype string) bool {
	contentType := r.Header.Get("Content-type")
	if contentType == "" {
		return mimetype == "application/json"
	}

	for _, v := range strings.Split(contentType, ",") {
		t, _, err := mime.ParseMediaType(v)
		if err != nil {
			break
		}
		if t == mimetype {
			return true
		}
	}
	return false
}

func getContentType(r *http.Request) (string, error) {
	if hasContentType(r, "application/xml") {
		return "xml", nil
	}
	if hasContentType(r, "application/json") {
		return "json", nil
	}

	return "", errors.New("unsupported content-type")
}

func contentTypeMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		contentType, err := getContentType(r)
		if err != nil {
			w.WriteHeader(http.StatusUnsupportedMediaType)
			w.Write([]byte("Supported media types: application/json and application/xml"))
			return
		}
		w.Header().Add("Content-Type", "application/" + contentType)
		// Call the next handler, which can be another middleware in the chain, or the final handler.
		next.ServeHTTP(w, r)
	})
}

func doRequest(uri string) *http.Response {
	// Build a request
	req, err := http.NewRequest("GET", ApiUrl + uri, nil)
	if err != nil {
		err := fmt.Sprintf("[%v] %v", ApiUrl + uri, err)
		panic(err)
	}

	// Perform the request
	resp, err := getHttpClient().Do(req)
	if err != nil {
		err := fmt.Sprintf("[%v] %v", ApiUrl + uri, err)
		panic(err)
	}

	return resp
}

func marshalList(bodyText []byte, contentType string) []byte {
	var data []interface{}
	json.Unmarshal(bodyText, &data)
	if contentType == "xml" {
		bodyText, _ = mxj.AnyXmlIndent(data, "", "", "body")
	}

	return bodyText
}

func marshalSingle(bodyText []byte, contentType string) []byte {
	var data map[string]interface{}
	json.Unmarshal(bodyText, &data)
	if contentType == "xml" {
		bodyText, _ = mxj.AnyXmlIndent(data, "", "", "body")
	}

	return bodyText
}

func proxify(list bool, resp *http.Response, w http.ResponseWriter, r *http.Request) ([]byte, error) {
	// Read the body text
	bodyText, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(fmt.Sprintf("%v", err))
	}
	if resp.StatusCode != http.StatusOK {
		w.WriteHeader(resp.StatusCode)
		return nil, errors.New("unexpected http status")
	}

	contentType, _ := getContentType(r)
	w.Header().Add("Content-Type", "application/" + contentType)

	if list {
		bodyText = marshalList(bodyText, contentType)
	} else {
		bodyText = marshalSingle(bodyText, contentType)
	}

	return bodyText, nil
}

func albumHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	resp := doRequest("/albums/" + vars["id"])
	defer resp.Body.Close()
	bodyText, err := proxify(false, resp, w, r)
	if err != nil {
		return
	}
	// Finally return as a string
	w.Write(bodyText)
}

func albumListHandler(w http.ResponseWriter, r *http.Request) {
	resp := doRequest("/albums")
	defer resp.Body.Close()
	bodyText, err := proxify(true, resp, w, r)
	if err != nil {
		return
	}
	// Finally return as a string
	w.Write(bodyText)
}

func userAlbumListHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	resp := doRequest("/users/"+vars["id"]+"/albums")
	defer resp.Body.Close()
	bodyText, err := proxify(true, resp, w, r)
	if err != nil {
		return
	}
	// Finally return as a string
	w.Write(bodyText)
}

func userListHandler(w http.ResponseWriter, r *http.Request) {
	resp := doRequest("/users")
	defer resp.Body.Close()
	bodyText, err := proxify(true, resp, w, r)
	if err != nil {
		return
	}
	// Finally return as a string
	w.Write(bodyText)
}

func userHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	resp := doRequest("/users/" + vars["id"])
	defer resp.Body.Close()
	bodyText, err := proxify(false, resp, w, r)
	if err != nil {
		return
	}
	// Finally return as a string
	w.Write(bodyText)
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/albums", albumListHandler).
		Name("album-list").
		Methods("GET")
	r.HandleFunc("/albums/{id:[0-9]+}", albumHandler).
		Name("album-list").
		Methods("GET")
	r.HandleFunc("/users", userListHandler).
		Name("user-list").
		Methods("GET")
	r.HandleFunc("/users/{id:[0-9]+}", userHandler).
		Name("user").
		Methods("GET")
	r.HandleFunc("/users/{id:[0-9]+}/albums", userAlbumListHandler).
		Name("user").
		Methods("GET")
	r.Use(contentTypeMiddleware)

	port := os.Getenv("PORT")
	if port == "" {
		port = "8777"
	}

	srv := &http.Server{
		Handler: r,
		Addr:    ":"+port,
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	// Run our server in a goroutine so that it doesn't block.
	go func() {
		fmt.Println("Binding to :" + port)
		if err := srv.ListenAndServe(); err != nil {
			log.Fatal(err)
		}
	}()

	c := make(chan os.Signal, 1)
	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	signal.Notify(c, os.Interrupt)

	// Block until we receive our signal.
	<-c

	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	// Doesn't block if no connections, but will otherwise wait
	// until the timeout deadline.
	srv.Shutdown(ctx)
	// Optionally, you could run srv.Shutdown in a goroutine and block on
	// <-ctx.Done() if your application should wait for other services
	// to finalize based on context cancellation.
	fmt.Println("Shutting down")
	os.Exit(0)
}