# How to build

1. Install golang [version 1.10.*](https://golang.org/dl/)
2. Install [dep](https://golang.github.io/dep/)
3. Clone the sources
4. Cd to the root directory of the sources
5. Run `dep ensure`
6. Run `./monster`
7. Do you calls to http://localhost:8777/*

To change the response format use `Content-Type` header. Supported content-types: `application/json`, `application/xml`.
If no content-type is given, `application/json` is assumed.

Supported endpoints:

* GET /users
* GET /users/:id
* GET /users/:id/albums
* GET /albums
* GET /albums/:id

There is a heroku hosted running demo (I use a free version of Heroku, and as such Heroku freezes unused apps, so the first call maybe slower than the next ones):

* https://monster-com.herokuapp.com/users
* https://monster-com.herokuapp.com/users/1
* https://monster-com.herokuapp.com/users/1/albums
* https://monster-com.herokuapp.com/albums
* https://monster-com.herokuapp.com/albums/1

P.S. This is a quick solution that lacks many error checks and also it should be decoupled to modules. But as a demo, I believe it's enough.
